const mongoose = require('mongoose')

const Setting = require('../models/Setting')

mongoose.connect("mongodb://localhost:27017/extrememine", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(() => {
    console.log('MONGO CONNECTION OPEN')
}).catch((err) => {
    console.log(err)
})

const seedSetting = [
    // {
    //     title: 'totalInvest',
    //     field: 52321654
    // },
    // {
    //     title: 'profitTimer',
    //     field: '04-12-2023'
    // },
    // {
    //     title: 'dailyProfit',
    //     field: 12
    // },
    {
        title: 'mainProfit',
        field: 24
    },
]

const seedDB = async () => {
    await Setting.insertMany(seedSetting)
}

seedDB().then(() => {
    console.log('data seed successfully')
    mongoose.connection.close()
    console.log('MONGO CONNECTION CLOSE')
})