const path = require("path")
const handlebars = require("handlebars")
const axios = require("axios")
const TronWeb = require("tronweb")
const fs = require('fs')

const tronWeb = new TronWeb({
    fullHost: "https://api.trongrid.io",
    headers: {"TRON-PRO-API-KEY": "b9041741-58bb-49c1-b28f-33121c80b3fb"},
    privateKey: process.env.ADMIN_PRIVATE_KEY,
})

const {generateRandomString, readHTMLFile, validation, addErrorLog} = require('../utils/funcs')
const checkTransaction = require('../jobs/checkTransactions')
const {sendMail} = require('../utils/mailer')
const User = require('../models/User')
const Asset = require('../models/Asset')
const Setting = require('../models/Setting')
const internalTransaction = require('../models/internalTransaction')
const InternalTransaction = require("../models/internalTransaction");

exports.dashboard = async (req, res) => {
    try {
        // let profit = await Setting.findOne({title: "dailyProfit"}).select('field')
        // let mainProfit = await Setting.findOne({title: "mainProfit"}).select('field')
        // let timer = await Setting.findOne({title: "profitTimer"}).select('field')
        // timer = (Date.parse(timer.field) / 1000) > (Date.now() / 1000) ? Date.parse(timer.field) / 1000 : null
        // await checkTransaction(req.user._id.toString())
        // let asset = await Asset.findOne({userId: req.user._id.toString()}).select('wallet amount profit referralProfit')
        let axios_api
        // try {
        //     axios_api = await axios.post('https://api.qrcode-monkey.com//qr/custom', {
        //         "data": asset.wallet.address.base58,
        //         "config": {
        //             "body": "circle",
        //             "eye": "frame1",
        //             "eyeBall": "ball1",
        //             "erf1": [
        //                 "fh"
        //             ],
        //             "erf2": [],
        //             "erf3": [
        //                 "fh",
        //                 "fv"
        //             ],
        //             "brf1": [
        //                 "fh"
        //             ],
        //             "brf2": [],
        //             "brf3": [
        //                 "fh",
        //                 "fv"
        //             ],
        //             "bodyColor": "#26A17B",
        //             "bgColor": "#FFFFFF",
        //             "eye1Color": "#26A17B",
        //             "eye2Color": "#26A17B",
        //             "eye3Color": "#26A17B",
        //             "eyeBall1Color": "#26A17B",
        //             "eyeBall2Color": "#26A17B",
        //             "eyeBall3Color": "#26A17B",
        //             "gradientColor1": "",
        //             "gradientColor2": "",
        //             "gradientType": "linear",
        //             "gradientOnEyes": false,
        //             "logo": "b0431056a87d514a3ac9c58586b8b2e6c08f39e7.png",
        //             "logoMode": "default"
        //         },
        //         "size": 300,
        //         "download": "imageUrl",
        //         "file": "svg"
        //     })
        // }catch (e) {
        //     await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in dashboard axios_api : ${e}\``)
        //     return false
        // }


        // let totalInvest = await Setting.findOne({title: "totalInvest"}).select('field')
        // totalInvest = totalInvest.field.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")

        res.render('users/dashboard', {
            pageTitle: "Dashboard",
            layout: "./layouts/user/main",
            user: req.user,
            // qrcode: axios_api.data.imageUrl,
            // asset,
            // totalInvest,
            // timer,
            // profit,
            // mainProfit
        })
    } catch (e) {
        console.log("error in dashboard", e)
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in dashboard : ${e}\``)
        return false
    }
}

exports.verifyEmail = async (req, res) => {
    if (req.user.verification === null) {
        return res.redirect('/users/dashboard')
    } else {
        const time = parseInt((new Date(req.user.timeVerification) - new Date(Date.now())) / (1000))
        res.render('users/verifyEmail', {
            pageTitle: "verify Email",
            layout: "./layouts/user/main",
            user: req.user,
            time
        })
    }
}

exports.resendVerifyEmail = async (req, res) => {
    let user = req.user
    const token = generateRandomString(100)
    let timeVerification = new Date()
    timeVerification.setMinutes(timeVerification.getMinutes() + 30)
    user.verification = token
    user.timeVerification = timeVerification
    user.save()
    try {
        readHTMLFile(path.join(__dirname, '../') + 'views/emails/verifyEmail.ejs', function (err, html) {
            if (err) {
                return res.status(500).send({status: 500, message: "something was wrong.please try later."})
            }
            let template = handlebars.compile(html);
            let replacements = {token}
            let htmlToSend = template(replacements);
            sendMail(user.email, 'verification', htmlToSend)
        })
        return res.status(200).send({
            status: 200,
            message: "Verify email was sent to your email.check your email",
            data: {reload: true}
        })
    } catch (err) {
        console.log(err)
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in resendVerifyEmail : ${err}\``)
        return res.status(500).send({status: 500, message: "Server Error! Please try later."})
    }

}

exports.subsets = async (req, res) => {
    try {
        const subsets = await User.find({registerCode: req.user.inviteCode}).select('email updatedAt')
        return res.render("users/subsets", {
            pageTitle: "Subsets",
            layout: "./layouts/user/main",
            user: req.user,
            subsets
        })
    } catch (e) {
        console.log("error in subsets", e)
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in subsets : ${e}\``)
        return false
        // return get500(res)
    }
}

exports.wallet = async (req, res) => {
    let asset = await Asset.findOne({userId: req.user._id.toString()}).select('wallet address amount profit referralProfit')
    const referrals = await User.find({registerCode: req.user.inviteCode}).count()
    let axios_api
    try {
        axios_api = await axios.post('https://api.qrcode-monkey.com//qr/custom', {
            "data": asset.wallet.address.base58,
            "config": {
                "body": "circle",
                "eye": "frame1",
                "eyeBall": "ball1",
                "erf1": [
                    "fh"
                ],
                "erf2": [],
                "erf3": [
                    "fh",
                    "fv"
                ],
                "brf1": [
                    "fh"
                ],
                "brf2": [],
                "brf3": [
                    "fh",
                    "fv"
                ],
                "bodyColor": "#26A17B",
                "bgColor": "#FFFFFF",
                "eye1Color": "#26A17B",
                "eye2Color": "#26A17B",
                "eye3Color": "#26A17B",
                "eyeBall1Color": "#26A17B",
                "eyeBall2Color": "#26A17B",
                "eyeBall3Color": "#26A17B",
                "gradientColor1": "",
                "gradientColor2": "",
                "gradientType": "linear",
                "gradientOnEyes": false,
                "logo": "b0431056a87d514a3ac9c58586b8b2e6c08f39e7.png",
                "logoMode": "default"
            },
            "size": 300,
            "download": "imageUrl",
            "file": "svg"
        })
    }catch (e) {
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in wallet axios_api : ${e}\``)
         return false
    }

    res.render('users/wallet', {
        pageTitle: "Wallet",
        layout: "./layouts/user/main",
        user: req.user,
        asset,
        referrals,
        qrcode: axios_api.data.imageUrl
    })
}

exports.withdraw = async (req, res) => {
    req.body.amount = parseFloat(req.body.amount)
    let {type, amount, address} = req.body

    const minWithdraw = type === 'asset' ? 100 : 100

    const schema = {
        amount: {type: "number", min: minWithdraw, positive: true, require},
        address: {
            type: "string",
            min: 34,
            max: 34,
            messages: {stringMin: "address must be from usdt trc20 ", stringMax: "address must be from usdt trc20"},
            require
        },
        'withdraw-password': {
            type: "string",
            messages: {empty: "withdraw password is required"},
            require,
            empty: false
        }
    }
    const validate = validation(req.body, schema)
    if (validate.length > 0) {
        return res.status(422).send({status: 422, errors: validate, data: {reload: true}})
    }

    try {
        const isMatch=req.user.password === req.body['withdraw-password'].trim()

        if (!isMatch) {
            return res.status(422).send({
                status: 422, errors: [{name: "withdraw-password", message: `withdraw password is not correct`}]
            })
        }

        let asset = await Asset.findOne({userId: req.user._id.toString()}).exec()
        const tableParam = type === 'asset' ? asset.amount : asset.profit

        if (!(tableParam >= amount)) {
            return res.status(422).send({
                status: 422, errors: [{name: "amount", message: `your ${type} amount is low`}]
            })
        }

        tronWeb.setPrivateKey(process.env.ADMIN_PRIVATE_KEY)
        let contract = await tronWeb.contract().at('TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t')
        const result = await contract.balanceOf(process.env.ADMIN_WALLET).call()
        const balance = tronWeb.toDecimal(result / 1000000)

        if (balance > amount) {
            let withdrawAmount
            let mainAmount
            const fee = type === 'asset' ? 20 : 5
            withdrawAmount = type === 'asset' ? asset.amount - amount : asset.profit - amount
            mainAmount = amount
            amount = (amount - (amount * fee / 100)).toFixed(2)
            amount = tronWeb.toSun(amount.toString())
            const resp = await contract.methods.transfer(address, amount).send()

            //? If Return True change status transaction to "5"
            if (resp) {
                await internalTransaction.create({
                    userId: req.user._id.toString(),
                    address: address,
                    txHash: resp,
                    amount: (Math.round(mainAmount * 100) / 100).toFixed(2),
                    type: `withdraw ${type}`,
                    status: 1
                })

                if (type === 'asset') {
                    asset.amount = parseFloat((Math.round(withdrawAmount * 100) / 100).toFixed(2))
                } else {
                    asset.profit = parseFloat((Math.round(withdrawAmount * 100) / 100).toFixed(2))
                }
                asset.updatedAt = new Date()
                asset.save()

                return res.status(200).send({
                    status: 200, message: 'withdraw submitted successfully', data: {reload: true}
                })
            } else {
                return res.status(500).send({
                    status: 500, message: 'something was wrong.please try later.'
                })
            }
        } else {
            readHTMLFile(path.join(__dirname, '../') + 'views/emails/adminEmail.ejs', function (err, html) {
                if (err) {
                    console.log(err)
                    return err
                }
                let template = handlebars.compile(html)
                let replacements = {amount, email: req.user.email, address: asset.wallet.address.base58}
                let htmlToSend = template(replacements)
                sendMail('ctrproject2020@gmail.com', 'charge admin wallet', htmlToSend)
            })
            return res.status(500).send({
                status: 500, message: 'something was wrong.please try 30 minutes later.'
            })
        }
    } catch (e) {
        console.log(e)
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in withdraw : ${e}\``)
        fs.appendFileSync('logs', `\r\n ${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in withdraw :  ${e}`)
        return res.status(500).send({
            status: 500,
            message: `error in ${type} withdrawal. please try later or contact with support.`,
            data: {reload: true}
        })
    }
}

exports.transactions = async (req, res) => {
    try {
        const transactions = await internalTransaction.find({userId: req.user._id.toString()}).sort({createdAt: -1})
        return res.render("users/transactions", {
            pageTitle: "transactions",
            layout: "./layouts/user/main",
            user: req.user,
            transactions
        })
    } catch (e) {
        console.log("error in history_transactions", e)
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in transactions : ${e}\``)
        return false
    }
}

exports.transferProfit = async (req, res) => {
    req.body.amount = parseFloat(req.body.amount)
    let {amount} = req.body
    const minWithdraw = 11

    const schema = {
        amount: {type: "number", min: minWithdraw, positive: true, require},
    }
    const validate = validation(req.body, schema)
    if (validate.length > 0) {
        return res.status(422).send({status: 422, errors: validate, data: {reload: true}})
    }

    try {
        let asset = await Asset.findOne({userId: req.user._id.toString()}).exec()

        if (!(asset.profit >= amount)) {
            return res.status(422).send({
                status: 422, errors: [{name: "amount", message: `your profit amount is low`}]
            })
        }

        let withdrawAmount
        let mainAmount
        const fee = 5
        withdrawAmount = asset.profit - amount
        mainAmount = amount
        amount = (amount - (amount * fee / 100)).toFixed(2)

        await internalTransaction.create({
            userId: req.user._id.toString(),
            address: asset.wallet.address.base58,
            txHash: null,
            amount: (Math.round(mainAmount * 100) / 100).toFixed(2),
            type: `transfer profit`,
            status: 1
        })

        if (amount > 10) {
            asset.amount = asset.amount + parseFloat((Math.round(amount * 100) / 100).toFixed(2))
            if (req.user.registerCode) {
                console.log('here')
                let leader = await User.findOne({"inviteCode": req.user.registerCode}).exec()
                let leaderAsset = await Asset.findOne({userId: leader._id.toString()}).exec()

                if (leaderAsset.amount >= 10) {
                    const referralProfitFee = 20
                    let profit = parseFloat(leaderAsset.profit) + (amount * referralProfitFee / 100)
                    let referralProfit = leaderAsset.referralProfit + (amount * referralProfitFee / 100)
                    leaderAsset.profit = parseFloat((Math.round(profit * 100) / 100).toFixed(2))
                    leaderAsset.referralProfit = parseFloat((Math.round(referralProfit * 100) / 100).toFixed(2))
                    leaderAsset.save()

                    await InternalTransaction.create({
                        userId: leader._id,
                        address: asset.wallet.address.base58,
                        txHash: null,
                        amount: parseFloat(amount * referralProfitFee / 100),
                        type: "referral",
                        //success status
                        status: 1
                    })
                }
            }
        }
        asset.profit = parseFloat((Math.round(withdrawAmount * 100) / 100).toFixed(2))
        asset.updatedAt = new Date()
        asset.save()

        return res.status(200).send({
            status: 200, message: 'transfer submitted successfully', data: {reload: true}
        })

    } catch (e) {
        console.log(e)
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in transferProfit : ${e}\``)
        return res.status(500).send({
            status: 500,
            message: `error in transfer. please try later or contact with support.`,
            data: {reload: true}
        })
    }
}