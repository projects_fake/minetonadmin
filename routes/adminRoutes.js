const {Router} = require('express')
const adminController = require('../controllers/adminController')
const {authenticated} = require('../middlewares/auth')
const {isAdmin} = require('../middlewares/isAdmin')


const router = new Router()

router.get('/dashboard',[authenticated,isAdmin],  adminController.dashboard)
router.get('/users',[authenticated,isAdmin],  adminController.users)
router.get('/transactions',[authenticated,isAdmin],  adminController.transactions)
router.get('/edit/:id',[authenticated,isAdmin],  adminController.editAsset)
router.post('/update-asset',[authenticated,isAdmin],  adminController.updateAsset)
router.get('/setting',[authenticated,isAdmin],  adminController.settingForm)
router.post('/setting',[authenticated,isAdmin],  adminController.setting)
router.post('/edit-profit',[authenticated,isAdmin],  adminController.editProfit)
router.post('/edit-main-profit',[authenticated,isAdmin],  adminController.editMainProfit)
router.post('/edit-withdraw-address',[authenticated,isAdmin],  adminController.withdrawAddress)
router.get('/force-withdraw',[authenticated,isAdmin],  adminController.forceWithdrawForm)
router.post('/estimate',[authenticated,isAdmin],  adminController.estimate)
router.post('/force-withdraw',[authenticated,isAdmin],  adminController.forceWithdraw)
router.get('/get-users',[authenticated,isAdmin],  adminController.getUsers)
router.get('/get-transactions',[authenticated,isAdmin],  adminController.getTransactions)
router.post('/delete-all',[authenticated,isAdmin],  adminController.deleteAll)

module.exports=router