const fs = require('fs')
const Validator = require("fastest-validator")
const validator = new Validator()

const logs=require('../models/errorLog')

exports.generateRandomString=(length)=>{
    let result= ''
    const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    const charactersLength = characters.length
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength))
    }
    return result
}

exports.get500=(res)=>{
    return res.render("errors/500",{pageTitle: "Server Error | 500",url:''})
}

exports.get404=(res)=>{
    return res.render("errors/404",{pageTitle: "Page Not Found | 404",url:''})
}

exports.getExpirePage=(res)=>{
    return res.render("errors/emailExpire",{pageTitle: "Page Not Found | 404",url:''})
}

//* this is for filter form above tables
exports.generateFilters=(keys,values)=>{
    for (let key of keys){
        values.hasOwnProperty(key) && values[key].length===0?delete values[key]:''
    }
    let amountFilter={}
    if (values.hasOwnProperty('from')) {
        amountFilter = {amount: {$gte: values.from}}
    }
    if (values.hasOwnProperty('to')) {
        if (amountFilter.hasOwnProperty('amount')) {
            amountFilter = {amount: {$gte: values.from, $lte: values.to}}
            delete values.from
        } else {
            amountFilter = {amount: {$lte: values.to}}
        }
        delete values.to
    }
    if (amountFilter.hasOwnProperty('amount'))
        values.amount=amountFilter.amount

    return values
}

//* this is for reading html view of emails files
exports.readHTMLFile = function(path, callback) {
    fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, html)
        }
    })
}

exports.validation = (body, schema) => {
    const check = validator.compile(schema)
    const validate = check(body)
    const errors = []
    if (validate !== true) {
        for (let error in validate) {
            errors.push({name:validate[error].field,message:validate[error].message})
        }
    }
    return errors
}

exports.addErrorLog=async function (path,error){
    try{
        await logs.create({path,error})
        return true
    }catch (e) {
        console.log(e)
        return
    }
}