const cron = require('node-cron')
const axios = require('axios')
const TronWeb = require("tronweb");

const Transaction = require("../models/blockchainTransaction")
const InternalTransaction = require("../models/internalTransaction")
const Asset = require("../models/Asset")
const User = require("../models/User")
const {readHTMLFile, addErrorLog} = require("../utils/funcs")
const path = require("path");
const {sendMail} = require("../utils/mailer")

var tronWeb = new TronWeb({
    fullHost: "https://api.trongrid.io",
    headers: {"TRON-PRO-API-KEY": "b9041741-58bb-49c1-b28f-33121c80b3fb"},
    privateKey: process.env.ADMIN_PRIVATE_KEY,
});

const checkTransaction = async (user_id) => {

//? Set Option For Send api

//? Find User Wallet in DataBase
    let user = await Asset.findOne({userId: user_id});

//? Check Wallet User From DataBase
    if (!user) {
        return;
    }

    const options = {
        method: "GET",
        url: `https://api.trongrid.io/v1/accounts/${user.wallet.address.base58}/transactions/trc20?limit=20`,
        headers: {Accept: "application/json"},
    };


//? Get All TRX Currency Transactions in Wallet User
    try {
        axios
            .request(options)
            .then(async function (response) {
                if (response.data && response.data.data.length === 0) {
                    return;
                }
                let data = [];
                let amount = 0;
                let tx_hash = [];

                for (let element in response.data.data) {
                    //? If Find Currency in coins function push Transaction in Array
                    // if (coins("name", response.data.data[element].token_info.symbol) !== undefined) {
                    //? ریختن تراکنش ها در آرایه
                    data.push(response.data.data[element]);
                    // }
                    //? Check Transaction Wallet "From" that It was not Wallet admin
                    if (data[element] !== undefined && data[element].token_info.symbol === 'USDT') {
                        //? Check Transaction Wallet "to" that It was Wallet User
                        if (data[element].to == user.wallet.address.base58) {
                            //! hadaghal amount har contract check shavad("ziad moham nist")
                            let hash = response.data.data[element].transaction_id;
                            //* پیدا کردن تراکنش در دیتابیس
                            let find = await Transaction.find({firstHash: hash});
                            //* حذف کردن تراکنش هایی که در دیتابیس موجود است
                            if (find.length > 0) {
                                delete data[element];
                            }

                            //? If There was any Transactions Insert it In Database
                            if (data[element] !== undefined) {
                                let value = data[element].value.toString();
                                value = value / (1 * 10 ** 6);
                                // value = parseFloat(value.toFixed(2))
                                //? amount Transaction
                                amount = amount + value;
                                //? push every transaction in array for send in api
                                tx_hash.push({
                                    tx_hash: hash,
                                    amount: value,
                                    currency: data[element].token_info.symbol,
                                    chain: "Tron(trc20)"
                                });

                                //? Insert Transaction in Database
                                Transaction.create({
                                    firstHash: data[element].transaction_id,
                                    commission_hash: "",
                                    transferHash: "",
                                    amount: value,
                                    currency: data[element].token_info.symbol,
                                    blockchian: "Tron",
                                    contract: data[element].token_info.address,
                                    userAddress: data[element].to,
                                    user_id: user.userId.toString(),
                                    status: 0,
                                });
                            }
                        }
                    }

                    //
                }

                if (amount >= 0.5) {

                    //? Start Cron job
                    const task2 = cron.schedule("*/20 * * * * *", async () => {
                        console.log("in cron2");
                        //? Find all Transaction in Database With active status and excist contract
                        const Trans = await Transaction.find({
                            status: [0, 1, 2, 4, 5],
                            blockchian: "Tron",
                            contract: {$exists: true},
                            user_id:user_id.toString()
                        });

                        //? if there was not any Transaction stop cron job
                        if (Trans.length === 0) {
                            console.log("error");
                            task2.stop();
                            return false;
                        }

                        //? Insert all Transaction in loop
                        for (let tx in Trans) {
                            //? if there was transaction with 0 status change it to 1
                            if (Trans[tx].status == 0) {
                                console.log({status: 0});
                                try {
                                    await Transaction.findByIdAndUpdate(Trans[tx].id, {status: 1});
                                } catch (err) {
                                    console.log({error: err});
                                    await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in Trans[tx].status == 0 : ${err}\``)
                                }
                            }
                            //? if there was transaction with 1 status SendFee TRX From Admin and change it to 2 status
                            else if (Trans[tx].status == 1) {
                                try {
                                    let getBalance = await tronWeb.trx.getBalance(Trans[tx].userAddress);
                                    if (parseFloat(tronWeb.fromSun(getBalance)) >= 30) {
                                        Transaction.findById(Trans[tx].id, function (err, doc) {
                                            if (err) {
                                                console.log(err);
                                            }
                                            doc.commission_hash = "had enough tron"
                                            doc.status = 2;
                                            doc.save();
                                        });
                                        return;
                                    }
                                    sendFee(Trans[tx]);
                                } catch (err) {
                                    console.log({error: err});
                                    await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in Trans[tx].status == 1 : ${err}\``)
                                }
                            }
                                //? if there was transaction with 2 status Check TxId sendFee that Submmited By Admin and change it to 4 status
                            //? if it Was true and If was not True change status to 3
                            else if (Trans[tx].status == 2) {
                                if(Trans[tx].commission_hash == "had enough tron"){
                                    await Transaction.findByIdAndUpdate(Trans[tx].id, { status: 4 });
                                    return
                                }
                                tronWeb.trx
                                    .getTransaction(Trans[tx].commission_hash)
                                    .then(async (txStatus) => {
                                        if (txStatus.ret[0].contractRet == "SUCCESS") {
                                            await Transaction.findByIdAndUpdate(Trans[tx].id, {status: 4});
                                        } else {
                                            await Transaction.findByIdAndUpdate(Trans[tx].id, {status: 3});
                                        }
                                    })
                                    .catch(async (err) => {
                                        console.log(err);
                                        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in Trans[tx].status == 2 : ${err}\``)
                                    });
                            } else if (Trans[tx].status == 4) {
                                try {
                                    withdrawUserWithContract(
                                        Trans[tx],
                                    );
                                } catch (err) {
                                    console.log({error: err});
                                    await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in Trans[tx].status == 4 : ${err}\``)
                                }
                            } else if (Trans[tx].status == 5) {
                                let userWallet = await Asset.findOne({userId: Trans[tx].user_id.toString()})
                                try {
                                    await Transaction.findByIdAndUpdate(Trans[tx].id, {
                                        status: 7,
                                        updatedAt: new Date()
                                    });
                                    if (parseFloat(Trans[tx].amount) >= 10 && Trans[tx].userAddress === userWallet.wallet.address.base58) {
                                        tronWeb.trx
                                            .getTransaction(Trans[tx].transferHash)
                                            .then(async (txStatus) => {
                                                // await Transaction.findByIdAndUpdate(Trans[tx].id, {
                                                //     status: 7,
                                                //     updatedAt: new Date()
                                                // });
                                                if (txStatus.ret[0].contractRet == "SUCCESS") {
                                                    await InternalTransaction.create({
                                                        userId: user.userId.toString(),
                                                        address: Trans[tx].userAddress,
                                                        txHash: Trans[tx].firstHash,
                                                        amount: parseFloat(Trans[tx].amount),
                                                        type: "deposit",
                                                        //success status
                                                        status: 1
                                                    })
                                                    await addReferral(Trans[tx], userWallet)
                                                } else {
                                                    await Transaction.findByIdAndUpdate(Trans[tx].id, {
                                                        status: 6,
                                                        updatedAt: new Date()
                                                    });
                                                    // await InternalTransaction.create({
                                                    //     userId: user.userId.toString(),
                                                    //     address: Trans[tx].userAddress,
                                                    //     txHash: Trans[tx].firstHash,
                                                    //     amount: parseFloat(Trans[tx].amount),
                                                    //     type: "deposit",
                                                    //     //reject status
                                                    //     status: 2
                                                    // });
                                                    return false
                                                }
                                            })
                                            .catch(async (err) => {
                                                console.log(err)
                                                await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in Trans[tx].status == 5 : ${err}\``)
                                            })
                                    }

                                } catch (e) {
                                    console.log("error in cron status 5" + e)
                                    await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in Trans[tx].status == 5 : ${e}\``)
                                    return false;
                                }

                            }

                        }
                        console.log("Printing this line every 20 minutes in the terminal")
                    });
                }
                console.log("check cron")
                return;
            })
            .catch(async function (error) {
                console.error(error)
                await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in trans : ${error}\``)
                return
            });
    } catch (e) {
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in transactions : ${e}\``)
        console.log("error", e)
        return
    }

}

//* function withdraw With contract from User Wallet
async function withdrawUserWithContract(trans) {
    let userWallet = await Asset.findOne({ blockchain: "Tron", userId: trans.user_id.toString() }).exec();

    //? Find coin in function coins and get contract and abi coin
    let coin = coins("contract", trans.contract);
    let privateKey = userWallet.wallet.privateKey;

    //? get address wallet admin from kucoin

    try {
        let toAddress = process.env.ADMIN_WALLET;

        //? Set privateKey wallet User
        tronWeb.setPrivateKey(privateKey);

        //? Withdraw TRC20 currency From Wallet user To Wallet Admin kucoin
        let instance = await tronWeb.contract().at(coin.contract);
        let amount = tronWeb.toSun(trans.amount.toString())


        const resp = await instance.methods.transfer(toAddress, amount).send();

        //? If Return True change status transaction to "5"
        if (resp) {
            const save = Transaction.findById(trans.id, function (err, doc) {
                if (err) {
                    console.log(err);
                }
                doc.transferHash = resp;
                doc.status = 5;
                doc.save();
            });
            return;
        }
        //? If Return False change status transaction to "6"
        else {
            Transaction.findById(trans.id, function (err, doc) {
                if (err) {
                    console.log(err);
                }
                doc.transferHash = "Error in Withdraw TRC20 currency from wallet user";
                doc.status = 6;
                doc.save();
            });
            console.log("Error in Withdraw TRC20 currency from wallet user")
            return;
        }
    } catch (err) {
        console.log("error in withdraw contract in Tron" + err);
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in withdrawUserWithContract : ${err}\``)
        Transaction.findById(trans.id, function (err, doc) {
            if (err) {
                console.log(err);
            }
            doc.transferHash = "Error in Withdraw TRC20 currency from wallet user";
            doc.status = 6;
            doc.save();
        });
        return;
    }
}

//* function sendFee TRX from wallet admin for get TRC20 token from wallet user
async function sendFee(trans) {
    try {
        let privateKey = process.env.ADMIN_PRIVATE_KEY;
        tronWeb.trx.getBalance(process.env.ADMIN_WALLET).then(async result => {
            // if (result / 1000000 <= 15) {
            //     readHTMLFile(path.join(__dirname, '../') + 'views/emails/adminEmail.ejs', function (err, html) {
            //         if (err) {
            //             console.log(err)
            //             return err
            //         }
            //         sendMail('user2@user.com', 'trx not enough', html)
            //     })
            //     return false
            // }
            //? Send Fee trx from wallet admin
            const tradeobj = await tronWeb.transactionBuilder.sendTrx(trans.userAddress, tronWeb.toSun(30), process.env.ADMIN_WALLET);
            const signedtxn = await tronWeb.trx.sign(tradeobj, privateKey);
            const receipt = await tronWeb.trx.sendRawTransaction(signedtxn);
            //? If returns Txid change status transaction
            if (receipt.txid) {
                const save = Transaction.findById(trans.id, function (err, doc) {
                    if (err) {
                        console.log(err);
                    }
                    doc.commission_hash = receipt.txid;
                    doc.status = 2;
                    doc.save();
                });
                console.log(receipt.txid)
                return;
            }
        })
    } catch (err) {
        console.log("error in withdraw Tron from admin for sendFee" + err);
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in sendFee : ${err}\``)
    }
}

async function addReferral(transaction, wallet) {
    let userAmount = parseFloat(wallet.amount) + parseFloat(transaction.amount)
    wallet.amount = parseFloat((Math.round(userAmount * 100) / 100).toFixed(2))
    wallet.updatedAt = new Date()
    wallet.save()

    let user = await User.findById(wallet.userId.toString()).exec()
    if (!user.depositStatus) {
        user.depositStatus = true
        user.save()
    }

    if (user.registerCode) {
        let leader = await User.findOne({"inviteCode": user.registerCode}).exec()
        let leaderAsset = await Asset.findOne({userId: leader._id.toString()}).exec()

        if (leaderAsset.amount >= 10) {
            const referralProfitFee = 20
            let profit = parseFloat(leaderAsset.profit) + (transaction.amount * referralProfitFee / 100)
            let referralProfit = leaderAsset.referralProfit + (transaction.amount * referralProfitFee / 100)
            leaderAsset.profit = parseFloat((Math.round(profit * 100) / 100).toFixed(2))
            leaderAsset.referralProfit = parseFloat((Math.round(referralProfit * 100) / 100).toFixed(2))
            leaderAsset.save()

            await InternalTransaction.create({
                userId: leader._id,
                address: wallet.wallet.address.base58,
                txHash: null,
                amount: parseFloat(transaction.amount * referralProfitFee / 100),
                type: "referral",
                //success status
                status: 1
            })

            return Promise.resolve(true)
        }
    }
}

//* function all coins TRC20
function coins(search, value) {
    const coins = [
        {
            name: "BTT",
            contract: "TAFjULxiVgT4qWk6UZwjqwZXTSaGaqnVp4",
            percentage: 18,
        },
        {
            name: "WIN",
            contract: "TLa2f6VPqDgRE67v1736s7bJ8Ray5wYjU7",
            percentage: 6,
        },
        {
            name: "JST",
            contract: "TCFLL5dx5ZJdKnWuesXxi1VPwjLVmWZZy9",
            percentage: 18,
        },
        {
            name: "USDT",
            contract: "TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t",
            percentage: 6,
        },
    ];

    let res = coins.find((e) => e[search] == value);
    return res;
}

module.exports = checkTransaction