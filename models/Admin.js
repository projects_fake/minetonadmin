const mongoose = require("mongoose")

const Schema = new mongoose.Schema({
    email: {
        type: String,
        trim:true,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
        minlength: 8,
        maxlength: 255,
    },
    isAdmin:{
        type:Boolean,
        default:false
    },
    createdAt: {
        type: Date,
        default: ()=> {
            return new Date()
        }
    },
    updatedAt: {
        type: Date,
        default: ()=>{return new Date()}
    },
})

const Admin = mongoose.model("Admin", Schema);

module.exports = Admin;
