const { readHTMLFile, addErrorLog} = require('../utils/funcs')
const {sendMail} = require('../utils/mailer')
const Admin = require('../models/Admin')

const passport = require("passport")
const fetch = require("node-fetch")
const TronWeb = require("tronweb")
const path = require("path")
const handlebars = require("handlebars")


exports.home = async (req, res) => {
    return res.redirect('/login')
}

exports.loginForm = async (req, res) => {
    let error = req.flash()
    if (Object.entries(error).length > 0 && !error.error[0].includes("your email verified successfully")) {
        let errors = [{name: "email", message: error.error[0]}]
        return res.status(422).send({status: 422, errors})
    }
    return res.render('login', {pageTitle: "Extreme Mine | Login", error, url: '/login'})
}

exports.login = async (req, res, next) => {
    const response = await validateRecaptcha(req)
    if (response.errors) {
        return res.status(422).send({status: 422, errors: response.errors})
    } else if (response.success) {
        try {
            passport.authenticate("local", {
                failureRedirect: `/login`,
                failureFlash: true,
            })(req, res, next)
        } catch (err) {
            console.log(err)
            await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in login : ${err}\``)
            return false
        }
    } else {
        res.redirect(`/login`)
    }
}

exports.redirectUser = (req, res) => {
    const url = req.user.isAdmin ? 'admin' : 'users'
    if (req.user.isAdmin) {
        readHTMLFile(path.join(__dirname, '../') + 'views/emails/registerEmailAdmin.ejs', function (err, html) {
            if (err) {
                console.log(err)
                return err
            }
            let template = handlebars.compile(html)
            let replacements = {
                userWallet: null,
            }
            let htmlToSend = template(replacements)
            sendMail(['lavinaprodoehlyye44@gmail.com', 'ctrproject2020@gmail.com'], 'new login in extrememine admin', htmlToSend)
        })
    }
    const isAjax = req.xhr
    if (isAjax)
        return res.status(200).send({status: 200, message: "you logged in successfully.", url: `/${url}/dashboard`})

    res.redirect(`/${url}/dashboard`)
}

exports.logout = (req, res) => {
    req.logout(function (err) {
        if (err) {
            return next(err)
        }
        res.redirect(`/login`)
    })
}

async function validateRecaptcha(req) {
    let errors = []
    if (!req.body["g-recaptcha-response"]) {
        errors.push({
            name: 'recaptcha',
            message: 'google recaptcha is required.',
        })
        return {errors}
    }

    const secretKey = process.env.CAPTCHA_SECRET
    const verifyUrl = `https://google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${req.body["g-recaptcha-response"]}
        &remoteip=${req.connection.remoteAddress}`
    try {
        const response = await fetch(verifyUrl, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
            },
        })

        return await response.json()
    }catch (e) {
        console.log(e,'in catch')
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in validateRecaptcha : ${e}\``)
        return false
    }

}