const User = require('../models/User')
const Asset = require('../models/Asset')
const internalTransaction = require('../models/internalTransaction')
const Setting = require('../models/Setting')
const {generateFilters, validation, get500, get404, addErrorLog} = require('../utils/funcs')

const TronWeb = require("tronweb")
const tronWeb = new TronWeb({
    fullHost: "https://api.trongrid.io",
    headers: {"TRON-PRO-API-KEY": "b9041741-58bb-49c1-b28f-33121c80b3fb"},
    privateKey: process.env.ADMIN_PRIVATE_KEY,
})

exports.dashboard = async (req, res) => {
    res.render('admin/dashboard', {
        pageTitle: "Dashboard",
        layout: "./layouts/admin/main",
        user: req.user,
    })
}

exports.users = async (req, res) => {
    try {
        return res.render("admin/users", {
            pageTitle: "Users List",
            layout: "./layouts/admin/main",
            user: req.user,
        })
    } catch (err) {
        console.log(err)
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in users : ${err}\``)
        get500(res)
    }
}

exports.getUsers = async (req, res) => {
    const page = req.query.page || 1
    const limit = parseInt(req.query.limit) || 10

    if (req.query.depositStatus) {
        req.query.depositStatus = req.query.depositStatus == 1 ? true : false
    }
    let filters = req.query
    delete filters.skip
    delete filters.limit
    delete filters.page
    filters.data === '' ? delete filters.data : false
    // filters['isAdmin'] = false
    filters = generateFilters(['telegramId', 'address', 'username', 'inviteCode', 'registerCode','amountUSDT'], filters)
    Object.keys(filters).map(k => filters[k] = typeof filters[k] == 'string' ? filters[k].trim() : filters[k])
    let users
    const skip = (Object.keys(filters).length > 1 && page == 1) ? 0 : (page - 1) * limit

    try {
        if (filters.hasOwnProperty('address')) {
            const address = filters.address
            delete filters.address
            users = await User.aggregate([
                {$sort: {createdAt: -1}},
                {$match: filters},
            ]).lookup({
                from: "assets",
                localField: "telegramId",
                foreignField: "telegramId",
                as: "asset"
            })
            users = users.filter(user => user.asset[0].wallet.address.base58 === address)
        } else if (filters.hasOwnProperty('amountUSDT')) {
            const amount = filters.amountUSDT
            delete filters.amountUSDT
            users = await User.aggregate([
                {$sort: {updatedAt: -1}},
                {$match: filters},
            ]).lookup({
                from: "assets",
                localField: "telegramId",
                foreignField: "telegramId",
                as: "asset"
            })
            if (page > 1)
                return false
            users = users.filter(user => user.asset[0].amountUSDT >= amount)
        }else{
            users = await User.aggregate([
                {$sort: {createdAt: -1}},
                {$match: filters},
                {$skip: skip},
                {$limit: limit},
            ]).lookup({
                from: "assets",
                localField: "telegramId",
                foreignField: "telegramId",
                as: "asset"
            })
        }
        let totalAmount = 0
        let totalTon = 0
        let totalReferral = 0
        let sendProfits = 0
        for (let user of users) {
            user.referrals = await User.find({registerCode: user.inviteCode}).count()
            user.allProfits = 0
            const trans = await internalTransaction.find({telegramId: user.telegramId, type: 'withdrawTON'})
            for (let tran of trans) {
                user.allProfits = user.allProfits + tran.amount
                tran.status ===1 ? sendProfits = sendProfits + tran.amount:''
            }
            const asset = await Asset.findOne({telegramId: user.telegramId})
            totalAmount = totalAmount + asset.amountUSDT

            totalTon = totalTon + asset.amountToken
            totalReferral = totalReferral + asset.amountReferral
        }
        res.json({users,
            totalAmount,
            totalTon,
            totalReferral,
            sendProfits,
        })
    } catch (e) {
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in getUsers : ${e}\``)
        return false
    }
}

exports.transactions = async (req, res) => {
    try {
        return res.render("admin/transactions", {
            pageTitle: "Transactions",
            layout: "./layouts/admin/main",
            user: req.user,
        })
    } catch (err) {
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in transactions : ${err}\``)
        return res.status(500).send({status: 500, message: "something was wrong! please try later."})
    }
}

exports.getTransactions = async (req, res) => {
    const page = req.query.page || 1
    const limit = parseInt(req.query.limit) || 10
    let filters = req.query
    delete filters.skip
    delete filters.limit
    delete filters.page
    filters.data === '' ? delete filters.data : false
    // filters['isAdmin'] = false
    filters = generateFilters(['telegramId', 'address', 'txHash', 'status', 'type'], filters)
    let transactions
    const skip = (Object.keys(filters).length > 1 && page == 1) ? 0 : (page - 1) * limit

    try {
        if (filters.hasOwnProperty('email')) {
            const email = filters.email
            delete filters.email
            transactions = await internalTransaction.find(filters).sort({createdAt: -1}).populate('telegramId')
            if (page > 1)
                return false
            transactions = transactions.filter(transaction => transaction.userId.telegramId === email)
        } else
            transactions = await internalTransaction.find(filters).skip(skip).limit(limit).populate('telegramId').sort({updatedAt: -1})

        return res.json(transactions)
    } catch (err) {
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in getTransactions : ${err}\``)
        return false
    }
}

exports.editAsset = async (req, res) => {
    try {
        const asset = await Asset.findById(req.params.id).populate('telegramId')
        return res.render('admin/editUserAsset', {
            pageTitle: "Edit Asset",
            layout: "./layouts/admin/main",
            user: req.user,
            asset
        })
    } catch (err) {
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in editAsset : ${err}\``)
        get404(res)
    }
}

exports.updateAsset = async (req, res) => {
    req.body.amountUSDT = parseFloat(req.body.amountUSDT)
    req.body.amountToken = parseFloat(req.body.amountToken)
    req.body.amountReferral = parseFloat(req.body.amountReferral)

    const schema = {
        amountUSDT: {type: "number", require, empty: false},
        amountToken: {type: "number", require, empty: false},
        amountReferral: {type: "number", require, empty: false},
    }
    const validate = validation(req.body, schema)
    if (validate.length > 0) {
        return res.status(422).send({status: 422, errors: validate})
    }
    try {
        const {amountUSDT, amountToken, amountReferral} = req.body
        let asset = await Asset.findById(req.body.assetId)
        asset.amountUSDT = parseFloat(amountUSDT)
        asset.amountToken = parseFloat(amountToken)
        asset.amountReferral = parseFloat(amountReferral)
        asset.save()
        return res.status(200).send({status: 200, message: "asset updated successfully", data: {reload: true}})
    } catch (err) {
        console.log(err)
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in updateAsset : ${err}\``)
        return res.status(500).send({status: 500, message: "something was wrong! please try later."})
    }
}

exports.settingForm = async (req, res) => {
    try {
        const tokenPrice = await Setting.findOne({title: "tokenPrice"}).select('field')
        const mineFee = await Setting.findOne({title: "mineFee"}).select('field')
        const withdrawAddress = await Setting.findOne({title: "withdrawAddress"}).select('field')
        return res.render('admin/setting', {
            pageTitle: "Setting",
            layout: "./layouts/admin/main",
            user: req.user,
            tokenPrice,
            mineFee,
            withdrawAddress
        })
    } catch (err) {
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in settingForm : ${err}\``)
        get404(res)
    }
}

exports.editProfit = async (req, res) => {
    req.body.mineFee = parseFloat(req.body.mineFee)
    const schema = {
        mineFee: {type: "number", require, empty: false, positive: true},
    }
    const validate = validation(req.body, schema)
    if (validate.length > 0) {
        return res.status(422).send({status: 422, errors: validate})
    }

    try {
        const {mineFee} = req.body
        await Setting.findOneAndUpdate({title: "mineFee"}, {field: mineFee})
        return res.status(200).send({status: 200, message: "mineFee updated successfully", data: {reload: true}})
    } catch (e) {
        console.log(e)
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in editProfit : ${e}\``)
        return res.status(500).send({status: 500, message: "something was wrong! please try later."})
    }
}

exports.editMainProfit = async (req, res) => {
    req.body.mainProfit = parseFloat(req.body.mainProfit)
    const schema = {
        mainProfit: {type: "number", require, empty: false, positive: true},
    }
    const validate = validation(req.body, schema)
    if (validate.length > 0) {
        return res.status(422).send({status: 422, errors: validate})
    }

    try {
        const {mainProfit} = req.body
        await Setting.findOneAndUpdate({title: "mainProfit"}, {field: mainProfit})
        return res.status(200).send({status: 200, message: "info updated successfully", data: {reload: true}})
    } catch (e) {
        console.log(e)
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in editMainProfit : ${e}\``)
        return res.status(500).send({status: 500, message: "something was wrong! please try later."})
    }
}

exports.withdrawAddress = async (req, res) => {
    const schema = {
        withdrawAddress: {
            type: "string",
            min: 34,
            max: 34,
            messages: {stringMin: "address must be from usdt trc20 ", stringMax: "address must be from usdt trc20"},
            require
        },
    }
    const validate = validation(req.body, schema)
    if (validate.length > 0) {
        return res.status(422).send({status: 422, errors: validate})
    }

    try {
        const {withdrawAddress} = req.body
        await Setting.findOneAndUpdate({title: "withdrawAddress"}, {field: withdrawAddress})
        return res.status(200).send({status: 200, message: "info updated successfully", data: {reload: true}})
    } catch (e) {
        console.log(e)
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in withdrawAddress : ${e}\``)
        return res.status(500).send({status: 500, message: "something was wrong! please try later."})
    }
}

exports.setting = async (req, res) => {
    req.body.tokenPrice = parseFloat(req.body.tokenPrice)

    const schema = {
        tokenPrice: {type: "number", require, empty: false, positive: true}
    }
    const validate = validation(req.body, schema)
    if (validate.length > 0) {
        return res.status(422).send({status: 422, errors: validate})
    }
    try {
        const {tokenPrice} = req.body
        let item = await Setting.findOne({title: "tokenPrice"})
        item.field = tokenPrice
        item.save()
        return res.status(200).send({status: 200, message: "tokenPrice updated successfully", data: {reload: true}})
    } catch (err) {
        console.log(err)
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in setting : ${err}\``)
        return res.status(500).send({status: 500, message: "something was wrong! please try later."})
    }
}

exports.forceWithdrawForm = async (req, res) => {
    try {
        return res.render('admin/forceWithdraw', {
            pageTitle: "Force Withdraw",
            layout: "./layouts/admin/main",
            user: req.user,
            total: null,
            balance: null
        })
    } catch (err) {
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in forceWithdrawForm : ${err}\``)
        get404(res)
    }
}

exports.estimate = async (req, res) => {
    req.body.amount = parseFloat(req.body.amount)
    const schema = {
        amount: {type: "number", require, empty: false, positive: true},
    }
    const validate = validation(req.body, schema)
    if (validate.length > 0) {
        return res.status(422).send({status: 422, errors: validate})
    }

    try {
        tronWeb.setPrivateKey(process.env.ADMIN_PRIVATE_KEY)
        let contract = await tronWeb.contract().at('TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t')
        const assets = await Asset.find({amountUSDT: {$gte: req.body.amount}})
        let total = 0
        for (let item of assets) {
            total = total + item.amountUSDT
        }

        const result = await contract.balanceOf(process.env.ADMIN_WALLET).call()
        let balance = tronWeb.toDecimal(result / 1000000)
        balance = balance - (balance * 0.3)

        return res.status(200).send({status: 200, data: {total, balance, amount: req.body.amount}})
    } catch (e) {
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in estimate : ${e}\``)
        return res.status(500).send({status: 500, message: "server error!"})
    }

}

exports.forceWithdraw = async (req, res) => {
    try {
        const address = await Setting.findOne({title: "withdrawAddress"}).select('field')
        tronWeb.setPrivateKey(process.env.ADMIN_PRIVATE_KEY)
        let contract = await tronWeb.contract().at('TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t')
        const assets = await Asset.find({amountUSDT: {$gte: req.body.amount}})

        for (let asset of assets) {
            let withdrawAmount

            withdrawAmount = (asset.amountUSDT - (asset.amountUSDT * 20 / 100)).toFixed(2)
            withdrawAmount = tronWeb.toSun(withdrawAmount.toString())
            const resp = await contract.methods.transfer(address.field, withdrawAmount).send()

            //? If Return True change status transaction to "5"
            if (resp) {
                await internalTransaction.create({
                    telegramId: asset.telegramId,
                    address: address.field,
                    txHash: resp,
                    amount: (Math.round(asset.amountUSDT * 100) / 100).toFixed(2),
                    type: `withdraw`,
                    status: 1
                })
                asset.amountUSDT = 0
                asset.updatedAt = new Date()
                asset.save()
            } else {
                console.log(resp)
                return false
            }
        }
        return res.status(200).send({status: 200, message: "done successfully", data: {reload: true}})
    } catch (e) {
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in forceWithdraw : ${e}\``)
        console.log(e)
        return false
    }
}

exports.deleteAll = async (req, res) => {
    let ids =req.body['ids[]']
    try {
        for (let id of ids){
            await Asset.findOneAndUpdate({userId:id},{amount:0})
        }
        return res.status(200).send({status: 200, message: "done successfully", data: {reload: true}})
    }catch (e) {
        console.log(e)
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in deleteAll : ${e}\``)
        return res.status(500).send({status: 500, message: "something was wrong! please try later."})
    }

}
