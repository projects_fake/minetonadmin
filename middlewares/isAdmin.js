exports.isAdmin = async (req, res, next) => {
    const user = req.user
    if (user.isAdmin) {
        return next()
    } else
        res.redirect("/404")
}