const mongoose = require("mongoose")

const Schema = new mongoose.Schema({
    wallet: {
        type: Object,
        required: true,
        unique: true,
    },
    amountUSDT: {
        type: Number,
        required: true,
        default: 0
    },
    amountToken: {
        type: Number,
        required: true,
        default: 0
    },
    amountReferral: {
        type: Number,
        required: true,
        default: 0
    },
    telegramId: {
        type: String,
        required:true,
        trim:true,
    },
    mineTime: {
        type: Date,
        default: () => {
            return new Date()
        }
    },
    createdAt: {
        type: Date,
        default: () => {
            return new Date()
        }
    },
    updatedAt: {
        type: Date,
        default: () => {
            return new Date()
        }
    },
})

const Asset = mongoose.model("Asset", Schema);

module.exports = Asset;
