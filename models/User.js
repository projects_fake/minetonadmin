const mongoose = require("mongoose")

const Schema = new mongoose.Schema({
    telegramId: {
        type: String,
        trim:true,
        required: true,
        unique: true,
    },
    username: {
        type: String,
        trim:true,
    },
    inviteCode: {
        type: String,
        length: 10,
        unique: true
    },
    registerCode: {
        type: String,
        length: 10,
    },
    step: {
        type: String,
        required: true,
    },
    data: {
        type: Array,
        required: false,
    },
    createdAt: {
        type: Date,
        default: ()=> {
            return new Date()
        }
    },
    updatedAt: {
        type: Date,
        default: ()=>{return new Date()}
    },
})


const User = mongoose.model("User", Schema);

module.exports = User;
