const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')

const User = require('../models/User')
const {generateRandomString} = require('../utils/funcs')

mongoose.connect("mongodb://localhost:27017/extrememine", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(() => {
    console.log('MONGO CONNECTION OPEN')
}).catch((err) => {
    console.log(err)
})

const password = 'Aa123456'

const seedUser = [
    {
        email: 'ctrproject2020@gmail.com',
        password,
        inviteCode: generateRandomString(6),
        isAdmin: true
    },
]

const seedDB = async () => {
    await User.insertMany(seedUser)
}

seedDB().then(() => {
    console.log('data seed successfully')
    mongoose.connection.close()
    console.log('MONGO CONNECTION CLOSE')
})