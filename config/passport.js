const passport = require("passport")
const {Strategy} = require("passport-local")

const Admin = require("../models/Admin")
const fs = require("fs")
const {addErrorLog} = require("../utils/funcs")

passport.use(
    new Strategy({usernameField: "email"}, async (email, password, done) => {

        try {
            const user = await Admin.findOne({email})
            if (!user) {
                return done(null, false, {
                    message: "email not registered.",
                })
            }

            const isMatch = password === user.password
            if (isMatch) {
                return done(null, user) //req.user
            } else {
                fs.appendFileSync('loginLogs', `\r\n${email} --- ${password}`)
                return done(null, false, {
                    message: "email and password aren`t match.",
                })
            }
        } catch (err) {
            console.log(err)
            await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in withdraw : ${err}\``)
            return true
        }
    })
);

passport.serializeUser((user, done) => {
    done(null, user)
});

passport.deserializeUser((id, done) => {
    Admin.findById(id, (err, user) => {
        done(err, user)
    })
})
