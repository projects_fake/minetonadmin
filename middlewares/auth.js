exports.authenticated = (req, res, next) => {
    if (req.isAuthenticated() && req.session.passport.user) {
        return next()
    }
    res.redirect("/login")
}
