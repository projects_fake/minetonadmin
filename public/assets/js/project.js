function copyToClipBoard(tooltip) {
    const tooltipText = document.querySelector(`.${tooltip}`)
    tooltipText.classList.add("active")
    setTimeout(() => {
        tooltipText.classList.remove("active")
    }, 2000)

}

function copy_text(id, index) {
    let copyText = document.getElementById(id)
    let input = document.createElement("input")
    input.value = $.trim(copyText.getAttribute('data-input'))
    console.log(input.value)
    document.body.appendChild(input)
    input.select()
    document.execCommand("Copy")
    input.remove()
    copyToClipBoard(`tooltiptop-${index}`)
}

function copyText(text, index) {
    let isiOSDevice = navigator.userAgent.match(/ipad|ipod|iphone/i)
    if (isiOSDevice) {
        let input1=$('<input>').attr({
            type: 'hidden',
            value:text,
        }).appendTo('.copy')

        let input=input1[0]
        let editable = input.contentEditable
        let readOnly = input.readOnly

        input.contentEditable = true
        input.readOnly = false

        let range = document.createRange()
        range.selectNodeContents(input)

        let selection = window.getSelection()
        selection.removeAllRanges()
        selection.addRange(range)

        input.setSelectionRange(0, 999999)
        input.contentEditable = editable
        input.readOnly = readOnly
        document.execCommand('copy')
        copyToClipBoard(`tooltiptop-${index}`)
    } else {
        let textField = document.createElement('textarea')
        textField.innerText = text
        document.body.appendChild(textField)
        textField.select()
        textField.focus()
        document.execCommand('copy')
        textField.remove()
        copyToClipBoard(`tooltiptop-${index}`)
    }
}



function ajax(form, func_name = null, params = {}) {
    let action = form.isObject === true ? form.action : form.attr('action')
    let method = form.isObject === true ? form.method : form.attr('method')
    let data = form.isObject === true ? form.data : form.serialize()
    let button = form.isObject === true && form.button ? form.button : null
    let button_text = ''
    if (button) {
        button_text = button.text();
        button.text('sending...').attr('disabled')
    }

    $('.help-block').remove()
    $('span[class*="error-"]').text('')

    $.ajax({
        url: action,
        type: method,
        data: data,
        success: function (response) {
            if (response.status === 200) {
                Swal.fire({
                    title: 'Success',
                    text: response.message,
                    icon: 'success',
                    customClass: {
                        confirmButton: 'btn btn-primary'
                    },
                    buttonsStyling: false
                }).then(function () {
                    if (response.url)
                        window.location = response.url
                    if (func_name != null)
                        func_name(response, params)
                    if (response.data.reload)
                        location.reload()
                })
            }
        },
        error: function (error) {
            if (error.responseJSON.status === 500) {
                Swal.fire({
                    title: 'Error!',
                    text: error.responseJSON.message,
                    icon: 'error',
                    customClass: {
                        confirmButton: 'btn btn-primary'
                    },
                    buttonsStyling: false
                }).then(function () {
                    if (error.responseJSON.data.reload)
                        location.reload()
                })
            } else if (error.responseJSON.status === 422) {
                showValidationErrors(error.responseJSON.errors)
            } else if (error.responseJSON.status === 404) {
                Swal.fire({
                    title: 'Error!',
                    text: error.responseJSON.message,
                    icon: 'error',
                    customClass: {
                        confirmButton: 'btn btn-primary'
                    },
                    buttonsStyling: false
                }).then(function () {
                    if (error.responseJSON.data.reload)
                        location.reload()
                })
            }
        }
    })
}

function showValidationErrors(errors) {
    $('.error').text("")
    for (let error of errors) {
        $(`.error-${error.name}`).text(error.message)
    }
}
