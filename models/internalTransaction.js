const mongoose = require("mongoose")

const Schema = new mongoose.Schema({
    telegramId: {
        type:  String,
        required: true,
        trim:true,
    },
    type: {
        type: String,
        enum: ['deposit', 'withdraw', 'withdrawTON','referral'],
        required: true,
    },
    address: {
        type: String,
        required: true,
        trim: true,
        maxlength: 100,
    },
    amount: {
        type: Number,
        required: true,
        default: 0
    },
    txHash: {
        type: String,
    },
    status: {
        type: Number,
        // status 0 => processing ,1 => success , 2 => reject
        enum: [0, 1, 2],
        default: 0
    },
    createdAt: {
        type: Date,
        default: () => {
            return new Date()
        }
    },
    updatedAt: {
        type: Date,
        default: () => {
            return new Date()
        }
    },
})

const internal_Transaction = mongoose.model("internal_Transaction", Schema);

module.exports = internal_Transaction;

