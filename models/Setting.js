const mongoose = require("mongoose")

const Schema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    field: {
        type: String,
        required: true,
    },
    createdAt: {
        type: Date,
        default: () => {
            return new Date()
        }
    },
    updatedAt: {
        type: Date,
        default: () => {
            return new Date()
        }
    },
})

const Settings = mongoose.model("Settings", Schema);

module.exports = Settings;
