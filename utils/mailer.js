const env=require('dotenv')
env.config()
const nodeMailer = require('nodemailer')
const smtp = require('nodemailer-smtp-transport')
const {addErrorLog} = require("./funcs")

const transporterDetails = smtp({
    // host: "smtp.mailtrap.io",
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    // port: 2525,
    // secure: true,
    secure: true,
    auth: {
        // user: "51c300ff849c3b",
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASSWORD,
        // pass: '86c8eb14bdc880'
    },
    tls: {
        rejectUnauthorized: false
    }
})

exports.sendMail = async (email, subject, html) => {
    const transporter = new nodeMailer.createTransport(transporterDetails)

    try {
        await transporter.sendMail({
            from: process.env.MAIL_FROM,
            to: email,
            subject: subject,
            html
        })
    } catch (e) {
        await addErrorLog(__filename,`${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in sendMail : ${e}\``)
        return false
    }
}
