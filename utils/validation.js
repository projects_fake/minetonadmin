const Validator = require("fastest-validator");
const validator = new Validator();

exports.validation = (body, schema) => {
    const check = validator.compile(schema)
    const validate = check(body)
    const errors = []
    if (validate !== true) {
        for (let error in validate) {
            errors.push({name:validate[error].field,message:validate[error].message})
        }
    }
    return errors
}