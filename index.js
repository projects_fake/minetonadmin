const express = require("express")
const bodyParser = require("body-parser")
const expressLayout = require("express-ejs-layouts")
const path = require("path")
const dotEnv = require("dotenv")
const MongoStore = require("connect-mongo")
const passport = require("passport")
const session = require("express-session")
const flash = require("connect-flash")

const authRoutes = require('./routes/authRoutes')
const adminRoutes = require('./routes/adminRoutes')
// * Load Config is for server
// dotEnv.config()
const connectDB = require("./config/db")

// * Load Config is for local
dotEnv.config({path: "./config/config.env"})


//* Database connection
connectDB()

//* Passport Configuration
require("./config/passport")
const cronjob = require("node-cron")
const Asset = require("./models/Asset")
const Setting = require("./models/Setting")
const internalTransaction = require("./models/internalTransaction")
const {addErrorLog} = require("./utils/funcs")

const app = express()

//* View Engine
app.use(expressLayout)
app.set("view engine", "ejs")
app.set("layout", "./layouts/landing/main")
app.set("views", "views")

app.use(bodyParser.urlencoded({extended: false}))

cronjob.schedule("30 04 * * *", async () => {
// cronjob.schedule("*/20 * * * * *", async () => {
    try {
        let mainProfit = await Setting.findOne({title: "mainProfit"}).select('field')
        mainProfit = parseFloat(mainProfit.field)
        console.log('run at midnight')
        let assets = await Asset.find({amount: {$gte: 10}}).select('amount profit').exec()
        for (let asset of assets) {
            let profit = asset.profit + (asset.amount * (mainProfit) / 100)
            asset.profit = parseFloat((Math.round(profit * 100) / 100).toFixed(2))
            asset.save()
        }
    } catch (e) {
        await addErrorLog(__filename, `${new Date().toLocaleDateString()} - ${new Date().toLocaleTimeString()} ==> error in midnight job : ${e}\``)
        console.log('error in middnight job', e)
        return false
    }
})

//* Session
app.use(
    session({
        secret: "secret",
        cookie: {maxAge: 2 * 60 * 60 * 1000},
        resave: false,
        saveUninitialized: false,
        store: MongoStore.create({mongoUrl: process.env.MONGO_URI, touchAfter: 24 * 3600}),
    })
)

//* Passport
app.use(passport.initialize())
app.use(passport.session())

app.use(flash())

//* Static Folder
app.use(express.static(path.join(__dirname, "public")))

app.use(authRoutes)
app.use('/admin/', adminRoutes)

//* 404 Page
app.use((req, res) => {
    res.render("errors/404", {pageTitle: "Page Not Found | 404", url: "/404"});
})

const Port = process.env.PORT || 3001
app.listen(Port, () => console.log(`Server started on port ${Port}...`))
