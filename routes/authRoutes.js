const {Router} = require('express')

const authController=require('../controllers/authContorller')

const router = new Router()

router.get('/', authController.home)

router.get('/login',authController.loginForm)
router.post('/login',authController.login,authController.redirectUser)

router.get('/logout', authController.logout)



module.exports=router
