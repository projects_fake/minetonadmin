const mongoose = require("mongoose");

const Schema = new mongoose.Schema({
    firstHash: {
        unique: true,
        type: String,
        required: true,
        trim: true,
        maxlength: 100,
    },
    //* اگر ارز اون شبکه بود کارمزدی نمیفرستیم و این ردیف رو خالی میذاریم
    commission_hash: {
        type: String,
        required: false,
        trim: true,
        maxlength: 100,
    },
    transferHash: {
        type: String,
        required: false,
        trim: true,
        maxlength: 100,
    },
    telegramId: {
        type: String,
        required: true,
        trim:true
    },
    userAddress: {
        type: String,
        required: true,
        trim: true,
        maxlength: 100,
    },
    amount: {
        type: Number,
        required: false,
    },
    //* 0=>new 1=>request withdraw admin 2=>check withdraw admin 3=>fail withdraw admin
    //*  4=>request withdraw user 5=>check withdraw 6=>fail withdraw user 7=>success
    status: {
        type: Number,
        min: 0,
        max: 7,
        required: true,
    },
    currency: {
        type: String,
        required: true,
        trim: true,
        maxlength: 10,
    },
    contract: {
        type: String,
        required: false,
        trim: true,
        maxlength: 100,
    },
    blockchain: {
        type: String,
        required: true,
        trim: true,
        maxlength: 10,
    },
    createdAt: {
        type: Date,
        required: true,
        default: () => {
            return new Date()
        },
    },
    updatedAt: {
        type: Date,
        required: true,
        default: () => {
            return new Date()
        },
    },
});

const Blockchain_Transactions = mongoose.model("Blockchain_Transactions", Schema);

module.exports = Blockchain_Transactions;
