const mongoose = require("mongoose")

const Schema = new mongoose.Schema({
    telegramId: {
        type: String,
        required: true,
        trim:true
    },
    source_currency: {
        type: String,
        required: true,
        maxlength: 15
    },
    destination_currency: {
        type: String,
        required: true,
        maxlength: 15
    },
    address: {
        type: String,
        required: true,
        trim: true,
        maxlength: 100,
    },
    status: {
        type: Number,
        // status 0 => processing ,1 => success , 2 => reject
        enum: [0, 1, 2],
        default: 0
    },
    amountUsdt: {
        type: Number,
        required: true,
        default: 0
    },
    amountToken: {
        type: Number,
        required: true,
        default: 0
    },
    fee: {
        type: Number,
        required: true,
        default: 0
    },
    createdAt: {
        type: Date,
        default: () => {
            return new Date()
        }
    },
    updatedAt: {
        type: Date,
        default: () => {
            return new Date()
        }
    },
})

const swap = mongoose.model("swap", Schema);

module.exports = swap;
